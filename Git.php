<?php

/**
 * Class W_Git
 * Helper
 */
class W_Git
{
    /**
     * @return string
     */
    public static function getCurrentCommit(): string
    {
        return exec('git rev-parse HEAD');
    }

    /**
     * @return string
     */
    public static function getCurrentBranch(): string
    {
        return exec('git rev-parse --abbrev-ref HEAD');
    }

    /**
     * @param string $fileName
     * @return string
     */
    public static function getLastCommitForFile(string $fileName): string
    {
        return exec('git log -n 1 --pretty=format:%H -- ' . $fileName);
    }

    /**
     * @param string $str
     * @return bool
     */
    public static function isValidGitHash(string $str): bool
    {
        return (bool) preg_match('/^[0-9a-f]{40}$/i', $str);
    }
}