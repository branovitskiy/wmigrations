<?php

/**
 * Interface InterfaceLogger
 */
interface InterfaceLogger
{
    /**
     * @param string $msg
     * @return void
     */
    public function info(string $msg);

    /**
     * @param string $msg
     * @return void
     */
    public function err(string $msg);
}